`./mvnw test` para ejecutar los tests.

`./mvnw exec:java -Dexec.mainClass="com.bernatespinas.backend_exam_1.BackendExam1Application"` para arrancar el servidor.

Para obtener la secuencia que empieza en el 300, por ejemplo, ir a `http://localhost:8080/fizz_buzz/300`.

La longitud de la secuencia está definida en `application.properties`.
