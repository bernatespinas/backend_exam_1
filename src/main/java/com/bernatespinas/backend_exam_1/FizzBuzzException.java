package com.bernatespinas.backend_exam_1;

public class FizzBuzzException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public FizzBuzzException(String message) {
		super(message);
	}

	public FizzBuzzException(String message, Throwable cause) {
		super(message, cause);
	}
}