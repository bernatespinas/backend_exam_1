// package com.bernatespinas.backend_exam_1;

// import javax.validation.constraints.Min;
// import javax.validation.constraints.NotNull;

// import org.springframework.boot.context.properties.ConfigurationProperties;
// import org.springframework.stereotype.Component;
// import org.springframework.validation.annotation.Validated;

// @Component
// @Validated
// @ConfigurationProperties(prefix = "app")
// public class PropertiesValidator {
// 	@NotNull
// 	@Min(1)
// 	private int fizz_buzz_sequence_length;

// 	public int getFizz_buzz_sequence_length() {
// 		return this.fizz_buzz_sequence_length;
// 	}

// 	public void setFizz_buzz_sequence_length(int fizz_buzz_sequence_length) {
// 		this.fizz_buzz_sequence_length = fizz_buzz_sequence_length;
// 	}
// }