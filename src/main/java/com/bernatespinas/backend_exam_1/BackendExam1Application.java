package com.bernatespinas.backend_exam_1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class BackendExam1Application {
	public static void main(String[] args) {
		SpringApplication.run(BackendExam1Application.class, args);
	}
}