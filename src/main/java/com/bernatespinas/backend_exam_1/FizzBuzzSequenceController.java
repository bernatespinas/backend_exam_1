package com.bernatespinas.backend_exam_1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FizzBuzzSequenceController {
	@Value("${app.fizz_buzz_sequence_length}")
	private int sequenceLength;

	private static final Logger LOGGER = LoggerFactory.getLogger(
		FizzBuzzSequenceController.class
	);

	/**
	 * Calculates the fizz buzz sequence beginning at `start` and with the length defined in `application.properties`.
	 * @param start The number at which the sequence has to start.
	 * @return The fizz buzz sequence.
	 * @throws FizzBuzzException
	 */
	@GetMapping("/fizz_buzz/{start}")
	public String getSequence(
		@PathVariable int start
	) throws FizzBuzzException {
		return getSequenceLength(start, sequenceLength);
	}

	/**
	 * Calculates the fizz buzz sequence beginning at `start` and with a specified length.
	 * @param start
	 * @param length
	 * @return The fizz buzz sequence.
	 * @throws FizzBuzzException
	 */
	public String getSequenceLength(
		int start,
		int length
	) throws FizzBuzzException {
		LOGGER.debug("env length is " + length);
		if (length <= 1) {
			throw new FizzBuzzException(String.format(
				"Invalid length: got %d, has to be greater than 1 (try checking app.fizz_buzz_sequence_length property)",
				length
			));
		}

		StringBuilder sequence = new StringBuilder();

		for (int i = start; i < start + length; i += 1) {
			boolean fizzOrBuzz = false;

			if (i % 3 == 0) {
				sequence.append("fizz");
				fizzOrBuzz = true;
			}

			if (i % 5 == 0) {
				sequence.append("buzz");
				fizzOrBuzz = true;
			}

			if (!fizzOrBuzz) {
				sequence.append(i);
			}

			sequence.append(' ');
		}

		String sequenceString = sequence.substring(0, sequence.length() - 1);

		LOGGER.info(sequenceString);

		return sequenceString;
	}
}