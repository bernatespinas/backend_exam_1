package com.bernatespinas.backend_exam_1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class FizzBuzzExceptionHandler {
	private static Logger LOGGER = LoggerFactory.getLogger(
		FizzBuzzExceptionHandler.class
	);

	@ExceptionHandler(value = {FizzBuzzException.class})
	public ResponseEntity<String> logException(Throwable throwable) {
		String message = throwable.getMessage();

		LOGGER.error(message);

		return ResponseEntity
			.status(HttpStatus.INTERNAL_SERVER_ERROR)
			.body("Exception: " + message)
		;
	}
}