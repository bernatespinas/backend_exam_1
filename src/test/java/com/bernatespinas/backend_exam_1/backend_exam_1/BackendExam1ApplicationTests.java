package com.bernatespinas.backend_exam_1.backend_exam_1;

import java.util.HashMap;
import java.util.Map.Entry;

import com.bernatespinas.backend_exam_1.FizzBuzzException;
import com.bernatespinas.backend_exam_1.FizzBuzzSequenceController;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class BackendExam1ApplicationTests {
	@Autowired
	private FizzBuzzSequenceController fizzBuzz;

	@Test
	void contextLoads() {
		assert this.fizzBuzz != null;
	}

	@Test
	void fizzBuzzValidateSequences() throws FizzBuzzException {
		HashMap<Integer, String> callResult = new HashMap<>();
		callResult.put(
			3,
			"fizz 4 buzz fizz 7 8 fizz buzz 11 fizz"
		);
		callResult.put(
			25,
			"buzz 26 fizz 28 29 fizzbuzz 31 32 fizz 34"
		);
		callResult.put(
			250,
			"buzz 251 fizz 253 254 fizzbuzz 256 257 fizz 259"
		);

		for (Entry<Integer, String> entry : callResult.entrySet()) {
			assert this.fizzBuzz
				.getSequenceLength(entry.getKey(), 10)
				.equals(entry.getValue())
			;
		}
	}

	@Test
	void fizzBuzzBottleneck() throws FizzBuzzException {
		for (int i = 0; i < 10_000; i += 1) {
			this.fizzBuzz.getSequenceLength(i, 1_000);
		}
	}

	@Test
	void fizzBuzzValidateLengthProperty() throws FizzBuzzException {
		this.fizzBuzz.getSequence(111);
	}
}